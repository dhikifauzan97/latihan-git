import java.util.Scanner;

import Interfaces.Phone;

public class MainApp {
	public static void main(String[] args) {
		//membuat object phone menggunakan instansi dari class Xiaomi
		Phone redmiNote10 = new Xiaomi();
		
		//membuat object phoneUser
		PhoneUser mia = new PhoneUser(redmiNote10);
		
		//Menyalakan HP
		mia.turnOnThePhone();
		
		//Membuat tampilan
		Scanner input = new Scanner(System.in);
		String aksi;
		boolean isLooping = true;
		
		do {
			System.out.println("====Aplikasi Interface====");
			System.out.println("[1] Nyalakan HP");
			System.out.println("[2] Matikan HP");
			System.out.println("[3] Perbesar Volume");
			System.out.println("[4] Perkecil Volume");
			System.out.println("[0] Keluar");
			System.out.println("==========================");
			
			System.out.println("Pilih Aksi");
			aksi = input.next();
			
			if (aksi.equals("1")) {
				mia.turnOnThePhone();
			}else if (aksi.equals("2")) {
				mia.turnOffThePhone();
			}else if (aksi.equals("3")) {
				mia.makePhoneLouder();
			}else if (aksi.equals("4")) {
				mia.makePhoneSilent();
			}else if (aksi.equals("0")) {
				isLooping = false;
			}
		} while (isLooping);
	}
}
