import Interfaces.Phone;

public class Xiaomi implements Phone{
	private int volume;
	private boolean isPowerOn;
	
	public Xiaomi () {
		//set volume awal atau default
		this.volume = 50;
	}
	
	@Override
	public void powerOn() {
		isPowerOn = true;
		System.out.println("Handphone menyala");
		System.out.println("Selamat datang di Xiaommi");
		System.out.println("Android version 10");
	}

	@Override
	public void powerOff() {
		isPowerOn = false;
		System.out.println("Matikan Handphone");
		
	}

	@Override
	public void volumeUp() {
		if (this.isPowerOn) {
			if (this.volume == MAX_VOLUME) {
				System.out.println("Volume sudah maksimal");
				System.out.println("Volume = " + this.volume + "%");
			}else {
				this.volume +=10;
				System.out.println("Volume Sekarang " + this.volume + "%");
			}
		}else {
			System.out.println("Handphone mati, silahkan menyalakan handphone terdahulu");
		}
		
	}

	@Override
	public void volumeDown() {
		if (this.isPowerOn) {
			if (this.volume == MIN_VOLUME) {
				System.out.println("Volume = " + this.volume + "%");
			}else {
				this.volume -=10;
				System.out.println("Volume Sekarang " + this.volume + "%");
			}
		}else {
			System.out.println("Handphone mati, silahkan menyalakan handphone terdahulu");
		}
		
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public boolean isPowerOn() {
		return isPowerOn;
	}

	public void setPowerOn(boolean isPowerOn) {
		this.isPowerOn = isPowerOn;
	}
	
	
	
	
}
